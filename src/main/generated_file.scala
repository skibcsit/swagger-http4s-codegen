package io.swagger.client.api

import java.text.SimpleDateFormat


import io.swagger.client.model._


import java.io.File
import io.swagger.client.ApiInvoker


import com.sun.jersey.multipart.FormDataMultiPart
import com.sun.jersey.multipart.file.FileDataBodyPart

import javax.ws.rs.core.MediaType

import java.io.File
import java.util.Date
import java.util.TimeZone

import scala.collection.mutable.HashMap

import com.wordnik.swagger.client._
import scala.concurrent.Future
import collection.mutable

import java.net.URI

import com.wordnik.swagger.client.ClientResponseReaders.Json4sFormatsReader._
import com.wordnik.swagger.client.RequestWriters.Json4sFormatsWriter._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

import org.json4s._

class SwaggerApi(
  val defBasePath: String = https://petstore.swagger.io/v2,
  defApiInvoker: ApiInvoker = ApiInvoker
) {
  private lazy val dateTimeFormatter = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
    formatter
  }
  private val dateFormatter = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
    formatter
  }
  implicit val formats = new org.json4s.DefaultFormats {
    override def dateFormatter = dateTimeFormatter
  }
  implicit val stringReader: ClientResponseReader[String] = ClientResponseReaders.StringReader
  implicit val unitReader: ClientResponseReader[Unit] = ClientResponseReaders.UnitReader
  implicit val jvalueReader: ClientResponseReader[JValue] = ClientResponseReaders.JValueReader
  implicit val jsonReader: ClientResponseReader[Nothing] = JsonFormatsReader
  implicit val stringWriter: RequestWriter[String] = RequestWriters.StringWriter
  implicit val jsonWriter: RequestWriter[Nothing] = JsonFormatsWriter

  var basePath: String = defBasePath
  var apiInvoker: ApiInvoker = defApiInvoker

  def addHeader(key: String, value: String): mutable.HashMap[String, String] = {
    apiInvoker.defaultHeaders += key -> value
  }

  val config: SwaggerConfig = SwaggerConfig.forUrl(new URI(defBasePath))
  val client = new RestClient(config)
  val helper = new AsyncHelper(client, config)

  def placeOrder (body: Order) = {
    val await = Try(Await.result(placeOrderAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def placeOrderAsync (body: Order) = {
    helper.placeOrder (body)
  }
  def findPetsByTags (tags: string) = {
    val await = Try(Await.result(findPetsByTagsAsync(tags), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def findPetsByTagsAsync (tags: string) = {
    helper.findPetsByTags (tags)
  }
  def addPet (body: Pet) = {
    val await = Try(Await.result(addPetAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def addPetAsync (body: Pet) = {
    helper.addPet (body)
  }
  def updatePet (body: Pet) = {
    val await = Try(Await.result(updatePetAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def updatePetAsync (body: Pet) = {
    helper.updatePet (body)
  }
  def getOrderById (orderId: Long) = {
    val await = Try(Await.result(getOrderByIdAsync(orderId), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def getOrderByIdAsync (orderId: Long) = {
    helper.getOrderById (orderId)
  }
  def deleteOrder (orderId: Long) = {
    val await = Try(Await.result(deleteOrderAsync(orderId), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def deleteOrderAsync (orderId: Long) = {
    helper.deleteOrder (orderId)
  }
  def logoutUser () = {
    val await = Try(Await.result(logoutUserAsync(), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def logoutUserAsync () = {
    helper.logoutUser ()
  }
  def getInventory () = {
    val await = Try(Await.result(getInventoryAsync(), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def getInventoryAsync () = {
    helper.getInventory ()
  }
  def createUsersWithListInput (body: List[User]) = {
    val await = Try(Await.result(createUsersWithListInputAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def createUsersWithListInputAsync (body: List[User]) = {
    helper.createUsersWithListInput (body)
  }
  def getUserByName (username: String) = {
    val await = Try(Await.result(getUserByNameAsync(username), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def getUserByNameAsync (username: String) = {
    helper.getUserByName (username)
  }
  def updateUser (username: String,
  body: User) = {
    val await = Try(Await.result(updateUserAsync(username, body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def updateUserAsync (username: String,
  body: User) = {
    helper.updateUser (username, body)
  }
  def deleteUser (username: String) = {
    val await = Try(Await.result(deleteUserAsync(username), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def deleteUserAsync (username: String) = {
    helper.deleteUser (username)
  }
  def loginUser (username: String,
  password: String) = {
    val await = Try(Await.result(loginUserAsync(username, password), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def loginUserAsync (username: String,
  password: String) = {
    helper.loginUser (username, password)
  }
  def createUser (body: User) = {
    val await = Try(Await.result(createUserAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def createUserAsync (body: User) = {
    helper.createUser (body)
  }
  def getPetById (petId: Long) = {
    val await = Try(Await.result(getPetByIdAsync(petId), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def getPetByIdAsync (petId: Long) = {
    helper.getPetById (petId)
  }
  def updatePetWithForm (petId: Long,
  name: Option[String] = None,
  status: Option[String] = None) = {
    val await = Try(Await.result(updatePetWithFormAsync(petId, name, status), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def updatePetWithFormAsync (petId: Long,
  name: Option[String] = None,
  status: Option[String] = None) = {
    helper.updatePetWithForm (petId, name, status)
  }
  def deletePet (api_key: Option[String] = None,
  petId: Long) = {
    val await = Try(Await.result(deletePetAsync(api_key, petId), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def deletePetAsync (api_key: Option[String] = None,
  petId: Long) = {
    helper.deletePet (api_key, petId)
  }
  def uploadFile (petId: Long,
  additionalMetadata: Option[String] = None,
  file: Option[File] = None) = {
    val await = Try(Await.result(uploadFileAsync(petId, additionalMetadata, file), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def uploadFileAsync (petId: Long,
  additionalMetadata: Option[String] = None,
  file: Option[File] = None) = {
    helper.uploadFile (petId, additionalMetadata, file)
  }
  def createUsersWithArrayInput (body: List[User]) = {
    val await = Try(Await.result(createUsersWithArrayInputAsync(body), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def createUsersWithArrayInputAsync (body: List[User]) = {
    helper.createUsersWithArrayInput (body)
  }
  def findPetsByStatus (status: string) = {
    val await = Try(Await.result(findPetsByStatusAsync(status), Duration.Inf))
    await match{
      case Success(i) => Some(await.get)
      case Failure(t) => None
    }
  }

  def findPetsByStatusAsync (status: string) = {
    helper.findPetsByStatus (status)
  }

}

class AsyncHelper(client: TransportClient, config: SwaggerConfig) extends ApiClient(client, config) {

  def placeOrder (body: Order)(implicit reader: ClientResponseReader[Order]) = {
        val path= (addFmt("/store/order"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def findPetsByTags (tags: string)(implicit reader: ClientResponseReader[list[Pet]]) = {
        val path= (addFmt("/pet/findByTags"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        queryParams += "tags" -> tags.toString


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def addPet (body: Pet)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/pet"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def updatePet (body: Pet)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/pet"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("PUT", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def getOrderById (orderId: Long)(implicit reader: ClientResponseReader[Order]) = {
        val path= (addFmt("/store/order/{orderId}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "orderId" + "\\}", orderId)))


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def deleteOrder (orderId: Long)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/store/order/{orderId}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "orderId" + "\\}", orderId)))


        val resFuture = client.submit("DELETE", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def logoutUser ()(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/logout"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def getInventory ()(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/store/inventory"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def createUsersWithListInput (body: List[User])(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/createWithList"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def getUserByName (username: String)(implicit reader: ClientResponseReader[User]) = {
        val path= (addFmt("/user/{username}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "username" + "\\}", username)))


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def updateUser (username: String,
    body: User)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/{username}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "username" + "\\}", username)))

        bodyParams = bodyParams + body


        val resFuture = client.submit("PUT", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def deleteUser (username: String)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/{username}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "username" + "\\}", username)))


        val resFuture = client.submit("DELETE", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def loginUser (username: String,
    password: String)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/login"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        queryParams += "username" -> username.toString

        queryParams += "password" -> password.toString


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def createUser (body: User)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def getPetById (petId: Long)(implicit reader: ClientResponseReader[Pet]) = {
        val path= (addFmt("/pet/{petId}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "petId" + "\\}", petId)))


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def updatePetWithForm (petId: Long,
    name: Option[String] = None,
    status: Option[String] = None)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/pet/{petId}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "petId" + "\\}", petId)))






        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def deletePet (api_key: Option[String] = None,
    petId: Long)(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/pet/{petId}"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        headerParams += "api_key" -> api_key.toString

        path.replaceAll("\\{" + "petId" + "\\}", petId)))


        val resFuture = client.submit("DELETE", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def uploadFile (petId: Long,
    additionalMetadata: Option[String] = None,
    file: Option[File] = None)(implicit reader: ClientResponseReader[ApiResponse]) = {
        val path= (addFmt("/pet/{petId}/uploadImage"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        path.replaceAll("\\{" + "petId" + "\\}", petId)))






        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def createUsersWithArrayInput (body: List[User])(implicit reader: ClientResponseReader[Unit]) = {
        val path= (addFmt("/user/createWithArray"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        bodyParams = bodyParams + body


        val resFuture = client.submit("POST", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }
  def findPetsByStatus (status: string)(implicit reader: ClientResponseReader[list[Pet]]) = {
        val path= (addFmt("/pet/findByStatus"))
        val queryParams = new mutable.HashMap[String, String]
        val headerParams = new mutable.HashMap[String, String]
        var bodyParams = ""

        queryParams += "status" -> status.toString


        val resFuture = client.submit("GET", path, queryParams.toMap, headerParams.toMap, bodyParams)

        resFuture flatMap { resp =>
            process(reader.read(resp))
        }

  }

}
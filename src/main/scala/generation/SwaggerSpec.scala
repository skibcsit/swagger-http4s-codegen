package generation



class SwaggerSpec(basePath:String, paths:Map[String,Any], definitions:Map[String,Any]) {
  def getBasePath = basePath

  private val pathList = {
    for((name,content) <- paths.toList.asInstanceOf[List[(String,Map[String,Any])]])
      yield new Path(name,content)
  }

  def getPaths = pathList

  private val defList= for((name,content) <- definitions.toList.asInstanceOf[List[(String,Map[String,Any])]])
      yield new Definition(name,content)

  def getDefinitions = defList

  override def toString = "SwaggerSpec(\n"+"basePath:"+basePath+"\nPaths:\n"+pathList+"\nDefinitions:\n"+defList+"\n)"
}

class Path(name:String,content:Map[String,Any]){
  def getName = name

  private val queriesList = for((name,content) <- content.toList.asInstanceOf[List[(String,Map[String,Any])]])
    yield new Query(name,content)

  def getQueries = queriesList

  override def toString: String = "Path(\n"+"Name:"+name + "\nQueries:\n" + queriesList +"\n)"
}


class Definition(name:String,content:Map[String,Any]){
  def getName = name

  val required = content.get("required").orNull.asInstanceOf[List[String]]

  def isRequired(name:String): Boolean = {
    if (required == null) return false
    for (item <- required){
      if (item.contentEquals(name)) return true
    }
    return false
  }

  private val propMap = content.get("properties").orNull.asInstanceOf[Map[String,Any]]

  private val propList = for((name,content) <- propMap.toList.asInstanceOf[List[(String,Map[String,Any])]])
    yield new Property(name,content)

  def getProperties = propList

  override def toString: String = "Definition(\n"+"Name:"+name + "\nRequired:" + required + "\nProperties:\n" + propList+"\n)"

}

class Query(name:String,content:Map[String,Any]){
  def getName = name.toUpperCase

  private val operationID = content.get("operationId").orNull.asInstanceOf[String]

  def getOperationId = operationID

  private val paramList = for(content <- content.get("parameters").orNull.asInstanceOf[List[Map[String,Any]]])
    yield new Parameter(content)

  def getParameters = paramList

  private val response200 = content.get("responses").getOrElse(null).asInstanceOf[Map[String,String]].get("200").getOrElse(null).asInstanceOf[Map[String,Any]]

  private val schemaResult = if(response200!=null) response200.get("schema").orNull else null
  private val typeResult = if(schemaResult!=null) schemaResult.asInstanceOf[Map[String,Any]].get("type").orNull else null
  private val itemResult = if(schemaResult!=null) schemaResult.asInstanceOf[Map[String,Any]].get("items").orNull else null

  def getResult:String = {
    if (response200 == null) return "Unit"
    if (typeResult == null){
      schemaResult.asInstanceOf[Map[String,String]].get("$ref").orNull.substring(14)
    }
    else{
      if ( typeResult.asInstanceOf[String].contentEquals("array")) "list["+itemResult.asInstanceOf[Map[String,String]].get("$ref").orNull.substring(14)+"]"
      else "Unit"
    }
  }

  override def toString: String = name + "***" + operationID + "***" + paramList
}


class Property(name:String, content: Map[String,Any]){
  def getName = name

  private val propType = content.get("type").getOrElse(content.get("$ref").orNull).asInstanceOf[String]

  private val item = content.get("items").orNull.asInstanceOf[Map[String,String]]

  private val itemType = if(item != null) item.get("type").getOrElse(item.get("$ref").orNull) else null

  private val formattedType = {
    propType match{
      case "integer" => "Long"
      case "string" => "String"
      case "boolean" => "Boolean"
      case "array" => itemType match{
        case "integer" => "List[Long]"
        case "string" => "List[String]"
        case "boolean" => "List[Boolean]"
        case _ => "List[" + itemType.substring(14) + "]"
      }
      case _ => propType.substring(14)
    }
  }

  def getType = formattedType

  override def toString: String = name + "***" + formattedType
}

class Parameter(content:Map[String,Any]){

  private val name = content.get("name").orNull.asInstanceOf[String]

  def getName = name

  private val in = content.get("in").orNull.asInstanceOf[String]

  def getIn = in

  private val required = content.get("required").orNull.asInstanceOf[Boolean]

  def getRequred = required

  private val simpleType = content.get("type").orNull.asInstanceOf[String]

  private val schema =  content.get("schema").orNull.asInstanceOf[Map[String,Any]]

  private val item = if (schema!=null) schema.get("items").orNull.asInstanceOf[Map[String,String]] else content.get("items").orNull.asInstanceOf[Map[String,Any]]

  private val itemType = if(item != null) item.get("$ref").orNull.asInstanceOf[String] else null

  private val complexType = if (schema!=null) schema.get("$ref").orNull.asInstanceOf[String] else null



  private val formattedType = {
    simpleType match{
      case "integer" => "Long"
      case "string" => "String"
      case "boolean" => "Boolean"
      case "file" => "File"
      case "array" => "List["+item.get("type").orNull.asInstanceOf[String].substring(0,1).toUpperCase()+item.get("type").orNull.asInstanceOf[String].substring(1)+"]"
      case _ => {
        if (complexType != null) complexType.substring(14)
        else "List["+itemType.substring(14)+"]"
      }
    }
  }


  def getType = formattedType

  override def toString: String = name + "***" + formattedType

  def getAppearance = if (required) name+": "+formattedType else name + ": Option["+formattedType+"] = None"

}
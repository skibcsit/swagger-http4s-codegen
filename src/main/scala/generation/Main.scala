package generation

import txt.{ApiClass, ModelClass}

object Main extends App{
  val json = new MyJsonParser("src/main/swagger.json")
  val paths = json.getPaths
  val definitions =json.getDefinitions
  val basePath = json.getBasePath
  val swag = new SwaggerSpec(basePath,paths,definitions)
  for (definition <- swag.getDefinitions){
    val code = ModelClass(definition)
    println(code.toString().trim())
  }
  val code = ApiClass(swag.getBasePath,swag.getPaths)
  print(code.toString())

}

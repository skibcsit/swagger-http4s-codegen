package generation

import scala.io.Source
import scala.util.parsing.json._

class MyJsonParser(filename:String) {
  val parsedFile= JSON.parseFull(Source.fromFile((filename)).mkString) match{
      case Some(i) =>  i.asInstanceOf[Map[String,Any]]
  }

  def getFromMap(m:Map[String,Any],key:String) = m.get(key) match{
    case Some(i) => i
  }

  def getBasePath={
    val schemes = getFromMap(parsedFile,"schemes").asInstanceOf[List[String]].head
    val host = getFromMap(parsedFile,"host").asInstanceOf[String]
    val basePath =getFromMap(parsedFile,"basePath").asInstanceOf[String]
    schemes+"://"+host+basePath
  }

  def getTags = for(item <- getFromMap(parsedFile,"tags").asInstanceOf[Array[Map[String,Any]]].toList)
    yield item.get("name").orNull.toString

  def getPaths=getFromMap(parsedFile,"paths").asInstanceOf[Map[String,Any]]

  def getDefinitions = getFromMap(parsedFile,"definitions").asInstanceOf[Map[String,Any]]

}


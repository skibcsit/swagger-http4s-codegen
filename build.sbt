name := "SwaggerClient"

version := "0.1"

scalaVersion := "2.12.6"

lazy val root = (project in file(".")).enablePlugins(SbtTwirl)

val circeVersion = "0.7.0"


libraryDependencies ++= Seq(
  "io.circe"  %% "circe-core"     % circeVersion,
  "io.circe"  %% "circe-generic"  % circeVersion,
  "io.circe"  %% "circe-parser"   % circeVersion
)
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.2.11"
libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"